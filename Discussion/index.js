// storage for posts
let posts = []; 

// counter for post id
let count = 1; 

// Add post event listener
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
  	// prevent default form behavior
	e.preventDefault();

  	// add post data to posts array
	posts.push({
    id: count, // increment id for each post
    title: document.querySelector('#txt-title').value, // get value of title field
    body: document.querySelector('#txt-body').value // get value of body field
});

  	// increment id for next post
	count++;

  	// show updated posts
	showPosts(posts);

  	// display success message
	alert('Successfully added');
});

// Show posts function
const showPosts = (posts) => {
  	// string to store HTML for each post
	let postEntries = '';

  	// loop through posts and create HTML for each post
	posts.forEach((post) => {
		postEntries += `
		<div id="post-${post.id}" style="display: inline-block; margin-right: 20px;">
		<h3 id="post-title-${post.id}">${post.title}</h3>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onclick="editPost('${post.id}')">Edit</button>
		<button onclick="deletePost('${post.id}')">Delete</button>
		</div>
		`
	});

  	// insert HTML into page
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};


// Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
};

// Update Post //Backend
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	for (let i = 0; i < posts.length; i++) {
		if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;
		}
	}

	showPosts(posts);
	alert('Successfully updated.');
});

// Delete Post
const deletePost = (id) => {
	for (let i = 0; i < posts.length; i++) {
		if (posts[i].id.toString() === id) {
			posts.splice(i, 1);
		}
	}

	showPosts(posts);
	alert('Successfully deleted.');
};
